﻿namespace SecureSoftwareDevelopmentPract1
{
    [Serializable]
    public class Person
    {
        public string Firstname
        {
            get;
            init;
        }
        public string Lastname
        {
            get;
            init;
        }
        public int? Age
        {
            get; init;
        }
        public string Country
        {
            get;
            init;
        }
        public string City
        {
            get;
            init;
        }
        public Person()
        {
            this.Firstname = "Не определено";
            this.Lastname = "Не определено";
            this.Age = 0;
            this.Country = "Не определено";
            this.City = "Не определено";

        }
        public Person(string firstname, string lastname, int age, string country, string city)
        {
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Age = age;
            this.Country = country;
            this.City = city;
        }
    }
}
