﻿using System.IO.Compression;
using System.Text;
using System.Text.Json;
using System.Xml.Serialization;

namespace SecureSoftwareDevelopmentPract1
{
    internal class Answer : Tasks
    {
        private Person person = new();
        public override Person? InputPerson 
        {
            get
            {
                return person;
            }
            set
            {
                if (value is not null)
                {
                    person = value;
                }
            }
        }
        private string inputString = "Текст не был введён.";
        public override string? InputString
        {
            protected get
            {
                return inputString;
            }
            set
            {
                if (!String.IsNullOrWhiteSpace(value))
                {
                    inputString = value;
                }
            }
        }
        public override string? FileName { get; set; }
        private List<Action> Methods { get; }

        public Answer() { 
            Methods = new List<Action>()
            {
                First,
                Second,
                Third,
                Fourth,
                Fifth
            };
        }
        public override bool CanSolveTask(int num)
        {
            if (num < 1 || num > Methods.Count)
            {
                return false;
            }
            return true;
        }
        public override void SolveTask(int num)
        {
            try
            {
                Methods[num].Invoke();
            }
            catch (IndexOutOfRangeException) 
            {
                Console.WriteLine("Ошибка. Под заданным номером задачи нет.");
            }
        }
        protected override void First()
        {
            DriveInfo[] drives = DriveInfo.GetDrives();

            foreach (DriveInfo drive in drives)
            {
                Console.WriteLine($"Название: {drive.Name}");
                Console.WriteLine($"\tТип: {drive.DriveType}");
                if (drive.IsReady)
                {
                    Console.WriteLine($"\tОбъем диска: {drive.TotalSize}B");
                    Console.WriteLine($"\tСвободное пространство: {drive.TotalFreeSpace}B");
                    Console.WriteLine($"\tМетка: {drive.VolumeLabel}");
                    Console.WriteLine($"\tИмя файловой системы: {drive.DriveFormat}");
                }
            }
        }
        protected override void Second()
        {
            FileName = FileNameInput("Second.txt");
            using (FileStream file = new(FileName, FileMode.Create, FileAccess.Write))
            {
                byte[] array = Encoding.UTF8.GetBytes(InputString!);
                file.Write(array, 0, array.Length);
                Console.WriteLine("Текст записан в файл. Нажмите любую клавишу...");
            }
            Console.ReadKey();
            try
            {
                using (FileStream file = new(FileName, FileMode.Open, FileAccess.Read))
                {
                    byte[] array = new byte[file.Length];
                    file.Read(array, 0, array.Length);
                    Console.WriteLine($"Текст из файла: {Encoding.UTF8.GetString(array)}. Нажмите любую клавишу...");
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Что-то пошло не так. Вероятно, файл был удалён или перемещён.");
            }
            Console.ReadKey();
            FileInfo fileInfo = new(FileName);
            if (fileInfo.Exists)
            {
                fileInfo.Delete();
                Console.WriteLine("Файл удалён.");
            }
        }
        protected override void Third()
        {
            string? fileName = FileNameInput("Third.json");
            using (FileStream file = new(fileName, FileMode.Create, FileAccess.Write))
            {
                JsonSerializer.Serialize(file, InputPerson);
                Console.WriteLine("Объект сериализован в файл. Нажмите любую клавишу...");
            }
            Console.ReadKey();
            try
            {
                using (FileStream file = new(fileName, FileMode.Open, FileAccess.Read))
                {
                    Person? restoredPerson = JsonSerializer.Deserialize<Person>(file) ?? throw new Exception();
                    Console.WriteLine(@$"Объект десериализован.
                        Имя: {restoredPerson.Firstname}
                        Фамилия: {restoredPerson.Lastname}
                        Возраст: {restoredPerson.Age}
                        Страна: {restoredPerson.Country}
                        Город: {restoredPerson.City}.");
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Что-то пошло не так. Вероятно, файл был удалён или перемещён.");
            }
            catch (JsonException) 
            {
                Console.WriteLine("Файл поломался...");
            }
            finally
            {
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey();
                FileInfo fileInfo = new(fileName);
                if (fileInfo.Exists)
                {
                    fileInfo.Delete();
                    Console.WriteLine("Файл удалён.");
                }
            }
        }
        protected override void Fourth()
        {
            FileName = FileNameInput("Fourth.xml");
            XmlSerializer xmlSerializer = new(typeof(Person));
            using (FileStream file = new(FileName, FileMode.Create, FileAccess.Write))
            {
                xmlSerializer.Serialize(file, InputPerson);
                Console.WriteLine("Объект сериализован в файл. Нажмите любую клавишу...");
            }
            Console.ReadKey();
            try
            {
                using (FileStream file = new(FileName, FileMode.Open, FileAccess.Read))
                {
                    Person? restoredPerson = xmlSerializer.Deserialize(file) as Person ?? throw new Exception();
                    Console.WriteLine(@$"Объект десериализован.
                        Имя: {restoredPerson.Firstname}
                        Фамилия: {restoredPerson.Lastname}
                        Возраст: {restoredPerson.Age}
                        Страна: {restoredPerson.Country}
                        Город: {restoredPerson.City}.");
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Что-то пошло не так. Вероятно, файл был удалён или перемещён.");
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Файл поломался...");
            }
            finally 
            {
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey();
                FileInfo fileInfo = new(FileName);
                if (fileInfo.Exists)
                {
                    fileInfo.Delete();
                    Console.WriteLine("Файл удалён.");
                }
            }
            
        }
        protected override void Fifth()
        {
            FileName = FileNameInput("Fifth.zip");
            try
            {   
                if (InputString is null)
                {
                    Console.WriteLine("По указанному пути не существует файла.");
                    return;
                }
                InputString = InputString.Trim('"');
                FileInfo fileInfo = new(InputString);
                string dirpath = fileInfo.DirectoryName!;
                using (var zipFile = ZipFile.Open(FileName, ZipArchiveMode.Create))
                {
                    zipFile.CreateEntryFromFile(InputString, Path.GetFileName(InputString));
                    fileInfo.Delete();
                    Console.WriteLine("Файл успешно запакован. Нажмите любую клавишу...");
                }
                Console.ReadKey();
                using (ZipArchive archive = ZipFile.OpenRead(FileName))
                {
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        Console.WriteLine(@$"Имя файла: {entry.Name}
                                    Размер файла: {entry.Length}Б
                                    Дата изменения: {entry.LastWriteTime}");
                    }
                }
                ZipFile.ExtractToDirectory(FileName, dirpath);
                Console.WriteLine("Файл успешно распакован.");
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Что-то пошло не так. Вероятно, файл был удалён или перемещён.");
            }
            catch (IOException)
            {
                Console.WriteLine("Что-то пошло не так. Вероятно, в указанном месте уже существует такой файл.");
            }
            catch (Exception)
            {
                Console.WriteLine("Неизвестная ошибка.");
            }
            finally
            {
                FileInfo fileinfo = new(FileName);
                if (fileinfo.Exists)
                {
                    fileinfo.Delete();
                }
            }
        }
        protected override string FileNameInput(string def)
        {
            string type = def.Substring(def.LastIndexOf("."));
            if (String.IsNullOrWhiteSpace(FileName))
            {
                FileName = def;
            }
            else if (FileName.Length < type.Length || !FileName.Substring(FileName.Length - 4).Equals(type))
            {
                FileName += type;
            }
            return FileName!;
        }
    }
}
